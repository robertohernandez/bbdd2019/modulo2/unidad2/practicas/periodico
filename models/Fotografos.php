<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fotografos".
 *
 * @property int $id
 * @property string $nombre
 * @property string $portfolio
 *
 * @property Fotografoemail[] $fotografoemails
 * @property Ilustran[] $ilustrans
 * @property Noticias[] $noticias
 */
class Fotografos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fotografos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'portfolio'], 'string', 'max' => 100],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'portfolio' => 'Portfolio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFotografoemails()
    {
        return $this->hasMany(Fotografoemail::className(), ['idfotografo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIlustrans()
    {
        return $this->hasMany(Ilustran::className(), ['idfotografo' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticias()
    {
        return $this->hasMany(Noticias::className(), ['id' => 'idnoticia'])->viaTable('ilustran', ['idfotografo' => 'id']);
    }
}
