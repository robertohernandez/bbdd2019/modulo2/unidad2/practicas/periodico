<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categorizan".
 *
 * @property int $id
 * @property int $idnoticia
 * @property int $idetiqueta
 *
 * @property Etiquetas $etiqueta
 * @property Noticias $noticia
 */
class Categorizan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorizan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idnoticia', 'idetiqueta'], 'integer'],
            [['idnoticia', 'idetiqueta'], 'unique', 'targetAttribute' => ['idnoticia', 'idetiqueta']],
            [['idetiqueta'], 'exist', 'skipOnError' => true, 'targetClass' => Etiquetas::className(), 'targetAttribute' => ['idetiqueta' => 'id']],
            [['idnoticia'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['idnoticia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idnoticia' => 'Idnoticia',
            'idetiqueta' => 'Idetiqueta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEtiqueta()
    {
        return $this->hasOne(Etiquetas::className(), ['id' => 'idetiqueta']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticia()
    {
        return $this->hasOne(Noticias::className(), ['id' => 'idnoticia']);
    }
}
