<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias".
 *
 * @property int $id
 * @property string $titulo
 * @property string $texto
 * @property int $idautor
 *
 * @property Categorizan[] $categorizans
 * @property Etiquetas[] $etiquetas
 * @property Ilustran[] $ilustrans
 * @property Fotografos[] $fotografos
 * @property Autores $autor
 * @property Noticiasfecha[] $noticiasfechas
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['texto'], 'string'],
            [['idautor'], 'integer'],
            [['titulo'], 'string', 'max' => 100],
            [['titulo'], 'unique'],
            [['idautor'], 'exist', 'skipOnError' => true, 'targetClass' => Autores::className(), 'targetAttribute' => ['idautor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'texto' => 'Texto',
            'idautor' => 'Idautor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategorizans()
    {
        return $this->hasMany(Categorizan::className(), ['idnoticia' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEtiquetas()
    {
        return $this->hasMany(Etiquetas::className(), ['id' => 'idetiqueta'])->viaTable('categorizan', ['idnoticia' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIlustrans()
    {
        return $this->hasMany(Ilustran::className(), ['idnoticia' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFotografos()
    {
        return $this->hasMany(Fotografos::className(), ['id' => 'idfotografo'])->viaTable('ilustran', ['idnoticia' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutor()
    {
        return $this->hasOne(Autores::className(), ['id' => 'idautor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasfechas()
    {
        return $this->hasMany(Noticiasfecha::className(), ['idnoticia' => 'id']);
    }
}
