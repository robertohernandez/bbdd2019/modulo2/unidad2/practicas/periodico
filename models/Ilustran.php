<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ilustran".
 *
 * @property int $id
 * @property int $idfotografo
 * @property int $idnoticia
 *
 * @property Fotografos $fotografo
 * @property Noticias $noticia
 */
class Ilustran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ilustran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idfotografo', 'idnoticia'], 'integer'],
            [['idfotografo', 'idnoticia'], 'unique', 'targetAttribute' => ['idfotografo', 'idnoticia']],
            [['idfotografo'], 'exist', 'skipOnError' => true, 'targetClass' => Fotografos::className(), 'targetAttribute' => ['idfotografo' => 'id']],
            [['idnoticia'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['idnoticia' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idfotografo' => 'Idfotografo',
            'idnoticia' => 'Idnoticia',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFotografo()
    {
        return $this->hasOne(Fotografos::className(), ['id' => 'idfotografo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticia()
    {
        return $this->hasOne(Noticias::className(), ['id' => 'idnoticia']);
    }
}
