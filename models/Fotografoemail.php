<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fotografoemail".
 *
 * @property int $id
 * @property int $idfotografo
 * @property string $email
 *
 * @property Fotografos $fotografo
 */
class Fotografoemail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fotografoemail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idfotografo'], 'integer'],
            [['email'], 'string', 'max' => 100],
            [['idfotografo', 'email'], 'unique', 'targetAttribute' => ['idfotografo', 'email']],
            [['idfotografo'], 'exist', 'skipOnError' => true, 'targetClass' => Fotografos::className(), 'targetAttribute' => ['idfotografo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idfotografo' => 'Idfotografo',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFotografo()
    {
        return $this->hasOne(Fotografos::className(), ['id' => 'idfotografo']);
    }
}
