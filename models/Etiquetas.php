<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "etiquetas".
 *
 * @property int $id
 * @property string $etiqueta
 *
 * @property Categorizan[] $categorizans
 * @property Noticias[] $noticias
 */
class Etiquetas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'etiquetas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['etiqueta'], 'string', 'max' => 50],
            [['etiqueta'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'etiqueta' => 'Etiqueta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategorizans()
    {
        return $this->hasMany(Categorizan::className(), ['idetiqueta' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNoticias()
    {
        return $this->hasMany(Noticias::className(), ['id' => 'idnoticia'])->viaTable('categorizan', ['idetiqueta' => 'id']);
    }
}
