<?php
    use yii\helpers\Html;
    use yii\helpers\ArrayHelper;
    
  
?>

  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <div class="caption">
        <h2><?= $model->titulo ?></h2>
        <p><?= $model->texto ?></p>
        <p><?= Html::a($model->autor->nombre,['autores/view','id'=>$model->id],['class' => 'btn btn-success']) ?></p>
        <p><?= implode('</p><p>',ArrayHelper::getColumn($model->noticiasfechas, 'fecha_publicacion')) ?></p>
        <p><?= implode('</p><p>',ArrayHelper::getColumn($model->etiquetas, 'etiqueta')) ?></p>
        
       
        
        
       
      </div>
    </div>
  </div>

