<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\ListView;
?>

<div class="jumbotron">
  <h2><?= $titulo ?></h2>

  
</div>

<p>
        <?= Html::a('Crear Noticia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

<div class="row">
<?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_vista1',
            'layout'=>"{summary}\n{pager}\n{items}",
        ]);
?>
</div>