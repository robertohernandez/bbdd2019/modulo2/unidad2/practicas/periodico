<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ilustran */

$this->title = 'Update Ilustran: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ilustrans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ilustran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
