<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Categorizan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="categorizan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idnoticia')->textInput() ?>

    <?= $form->field($model, 'idetiqueta')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
