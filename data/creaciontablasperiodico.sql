﻿DROP DATABASE IF EXISTS periodico;
CREATE DATABASE periodico;

USE periodico;

CREATE OR REPLACE TABLE autores(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  alias varchar(50),
  email varchar(150),
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE etiquetas(
  id int AUTO_INCREMENT,
  etiqueta varchar(50),
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE fotografos(
  id int AUTO_INCREMENT,
  nombre varchar(100),
  portfolio varchar(100),
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE fotografoemail(
  id int AUTO_INCREMENT,
  idfotografo int,
  email varchar(100),
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE noticias(
  id int AUTO_INCREMENT,
  titulo varchar(100),
  texto text,
  idautor int,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE noticiasfecha(
  id int AUTO_INCREMENT,
  idnoticia int,
  fecha_publicacion date,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE categorizan(
  id int AUTO_INCREMENT,
  idnoticia int,
  idetiqueta int,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE ilustran(
  id int AUTO_INCREMENT,
  idfotografo int,
  idnoticia int,
  PRIMARY KEY(id)
);

CREATE OR REPLACE TABLE usuarios(
  id int AUTO_INCREMENT, 
  login varchar(50),
  password varchar(50),
  PRIMARY KEY(id)
);

ALTER TABLE autores
  ADD CONSTRAINT uk_autores_nombre
  UNIQUE KEY(nombre);

ALTER TABLE etiquetas
  ADD CONSTRAINT uk_etiquetas_etiqueta
  UNIQUE KEY(etiqueta);

ALTER TABLE fotografos
  ADD CONSTRAINT uk_fotografos_nombre
  UNIQUE KEY(nombre);

ALTER TABLE fotografoemail
  ADD CONSTRAINT fk_fotografo_email
  FOREIGN KEY(idfotografo) REFERENCES fotografos(id),
  ADD CONSTRAINT uk_fotografoemail_fotografo_email
  UNIQUE KEY(idfotografo,email);

ALTER TABLE noticias
  ADD CONSTRAINT fk_noticias_autores
  FOREIGN KEY(idautor) REFERENCES autores(id),
  ADD CONSTRAINT uk_noticias_titulo
  UNIQUE KEY (titulo);

ALTER TABLE noticiasfecha
  ADD CONSTRAINT fk_noticiasfecha_fecha
  FOREIGN KEY(idnoticia) REFERENCES noticias(id),
  ADD CONSTRAINT uk_noticias_fecha
  UNIQUE KEY (idnoticia, fecha_publicacion);

ALTER TABLE categorizan
  ADD CONSTRAINT fk_categorizan_etiquetan
  FOREIGN KEY(idetiqueta) REFERENCES etiquetas(id),
  ADD CONSTRAINT fk_categorizan_noticia
  FOREIGN KEY(idnoticia) REFERENCES noticias(id),
  ADD CONSTRAINT uk_categorizan_etiqueta_noticias
  UNIQUE KEY (idnoticia,idetiqueta);

ALTER TABLE ilustran
  ADD CONSTRAINT fk_ilustran_fotografos
  FOREIGN KEY(idfotografo) REFERENCES fotografos(id),
  ADD CONSTRAINT fk_ilustran_noticias
  FOREIGN KEY(idnoticia) REFERENCES noticias(id),
  ADD CONSTRAINT uk_ilustran_noticias_fotografos
  UNIQUE KEY (idfotografo,idnoticia);
  
